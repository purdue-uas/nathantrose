## Welcome DO it! Camp!

### Welcome to Purdue!

The School of Aviation and Transportation Technology's UAS department looks forward to hosting you this Friday, 04 March 2022! We're going to build, fly, simulate, and of course give away some Purdue UAS swag!

### Before you Arrive

We have lots planned, but little time! So that we hit the skies flyin' please take a few moments to download the DJI Go4 app. Here's how:

1. Blah. 
   ![img](/img/hexagon.jpg)

2. Then do this thing:
